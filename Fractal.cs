﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

/* Fractal
 * -------
 * 
 * Full program conversion from Java to C#
 * Original Java Program drew a zoomable Fractal
 * Original Java Program supplied by DJM 
 * 
 * Extended Features:
 *  - Copy to Clipboard
 *  - Save State
 *  - Load State
 *  - Export
 *  - Color Cycling
 * 
 * @Author: Alex McCormick
 * @Alias: amccmick1
 * @Student ID: c3341430
 * @Date: 08/11/14
 * @Version: 0.1.7
 * 
 * Full source code and version control available here to those with permissions:
 *  - https://bitbucket.org/amccmick1/asea-fractal/
 */

namespace Fractal
{
    public partial class Fractal : Form
    {

        private static bool action, rectangle, mouseDragged;                    // mouseDragged bool is flag to replace built in Java function MouseDragged
        private static int x1, y1, xs, xe, ys, ye;
        private static double xstart, ystart, xende, yende, xzoom, yzoom;
        private const int MAX = 256;                                            // max iterations
        private const double SX = -2.025;                                       // start value real
        private const double SY = -1.125;                                       // start value imaginary
    	private const double EX = 0.6;                                          // end value real
        private const double EY = 1.125;                                        // end value imaginary
        private static float xy;
        private Bitmap picture;                                                 // bitmap of the fractal
        private Graphics g1;
        private HSBColor HSBcol = new HSBColor();                               // HSBColor provided by DJM converts RGB colors to HSB
        private Pen p;                                                          // Pen used to draw lines to bitmap replaces Java canvas set color

        public Fractal()
        {
            InitializeComponent();
            init();
            start();
        }

        [STAThread]
        static void Main()
        {
            Application.Run(new Fractal());
        }

        public void init() // all instances will be prepared
        {
            // Get width and height of form 
            // (replaces setSize java method)
            x1 = this.ClientRectangle.Width;
            y1 = this.ClientRectangle.Height;

            xy = (float)x1 / (float)y1;

            // Create Bitmap to draw to GDI
            picture = new Bitmap(x1, y1);
            g1 = Graphics.FromImage(picture);
        }

        public void start()
        {
            action = false;
            rectangle = false;
            initvalues();
            xzoom = (xende - xstart) / (double)x1;
            yzoom = (yende - ystart) / (double)y1;
            mandelbrot();
        }

        private void initvalues() // reset start values
        {
            xstart = SX;
            ystart = SY;
            xende = EX;
            yende = EY;
            if ((float)((xende - xstart) / (yende - ystart)) != xy)
                xstart = xende - (yende - ystart) * (double)xy;
        }

        private void mandelbrot() // calculate all points
        {
            int x, y;
            float h, b, alt = 0.0f;
            Color myColor;

            action = false;

            this.Cursor = Cursors.WaitCursor;   // Set Wait Cursor while it's loading

            for (x = 0; x < x1; x += 2)
                for (y = 0; y < y1; y++)
                {
                    h = pointcolour(xstart + xzoom * (double)x, ystart + yzoom * (double)y); // color value
                    if (h != alt)
                    {
                        b = 1.0f - h * h; // brightnes
                        HSBcol = new HSBColor(h, 0.8f, b);
                        myColor = HSBcol.FromHSB(HSBcol);
                        p = new Pen(myColor);
                        alt = h;
                    }
                    g1.DrawLine(p, x, y, x + 1, y);
                }

            this.Cursor = Cursors.Cross;    // Set Crosshair Cursor

            action = true;
        }

        private float pointcolour(double xwert, double ywert) // color value from 0.0 to 1.0 by iterations
        {
            double r = 0.0, i = 0.0, m = 0.0;
            int j = 0;

            while ((j < MAX) && (m < 4.0))
            {
                j++;
                m = r * r - i * i;
                i = 2.0 * r * i + ywert;
                r = m + xwert;
            }
            return (float)j / (float)MAX;
        }

        private Bitmap colorcycle(Bitmap bitmap)    // converts bitmap fractal to gif and pushes the palete entries pointer along one
        {
            MemoryStream stream = new MemoryStream();
            bitmap.Save(stream, ImageFormat.Gif);
            Bitmap colorcycleBitmap = new Bitmap(stream);
            ColorPalette palette = colorcycleBitmap.Palette;
            for (int i = 0; i < (palette.Entries.Length - 5); i++)
            {
                Color color = palette.Entries[i];
                palette.Entries[i] = palette.Entries[i + 1];
                palette.Entries[i + 1] = color;
            }
            colorcycleBitmap.Palette = palette;
            stream.Close();
            
            return colorcycleBitmap;
        }

        public void paint(Graphics g)
        {
            update(g);
        }

        public void update(Graphics g)
        {
            g.DrawImage(picture, 0, 0);
            if (rectangle)
            {
                p = new Pen(Color.White);
                if (xs < xe)
                {
                    if (ys < ye) g.DrawRectangle(p, xs, ys, (xe - xs), (ye - ys));
                    else g.DrawRectangle(p, xs, ye, (xe - xs), (ys - ye));
                }
                else
                {
                    if (ys < ye) g.DrawRectangle(p, xe, ys, (xs - xe), (ye - ys));
                    else g.DrawRectangle(p, xe, ye, (xs - xe), (ys - ye));
                }
            }
        }

        public void Form1_Paint(object sender, PaintEventArgs e) // called by Form1 -> Events :: Appearence : Paint
        {
            paint(e.Graphics);
        }

        public void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if (action)
            {
                xs = e.X;
                ys = e.Y;
                mouseDragged = true; 
            }
        }

       public void Form1_MouseMove(object sender, MouseEventArgs e)
       {
           if (mouseDragged == true)
           {
               if (action)
               {
                   xe = e.X;
                   ye = e.Y;
                   rectangle = true;
                   Refresh();
               }
           }
        }

       public void Form1_MouseUp(object sender, MouseEventArgs e)
       {
           int z, w;

           if (action)
           {
               xe = e.X;
               ye = e.Y;
               if (xs > xe)
               {
                   z = xs;
                   xs = xe;
                   xe = z;
               }
               if (ys > ye)
               {
                   z = ys;
                   ys = ye;
                   ye = z;
               }
               w = (xe - xs);
               z = (ye - ys);
               if ((w < 2) && (z < 2)) initvalues();
               else
               {
                   if (((float)w > (float)z * xy)) ye = (int)((float)ys + (float)w / xy);
                   else xe = (int)((float)xs + (float)z * xy);
                   xende = xstart + xzoom * (double)xe;
                   yende = ystart + yzoom * (double)ye;
                   xstart += xzoom * (double)xs;
                   ystart += yzoom * (double)ys;
               }
               xzoom = (xende - xstart) / (double)x1;
               yzoom = (yende - ystart) / (double)y1;
               mandelbrot();
               rectangle = false;
               mouseDragged = false;
               Refresh();
           }
       }

       private void copyToClipboardToolStripMenuItem_Click(object sender, EventArgs e)  // saves image to clipboard
       {
           Clipboard.SetImage(picture);
       }

       private void saveStateToolStripMenuItem_Click(object sender, EventArgs e)    // save the four variables required by mandlebrot to draw fractal in array and write array to file
       {
           string[] lines = { "xzoom = " + xzoom, "yzoom = " + yzoom, "xstart = " + xstart, "ystart = " + ystart};
           System.IO.File.WriteAllLines(@"C:\Users\Public\Fractal.save", lines);
       }

       private void loadStateToolStripMenuItem_Click(object sender, EventArgs e)    // convert strings in file outputted by save state to variables using substring to fetch value from "xzoom  = "[value] etc...
       {
           String line = "";

           try
           {
                StreamReader sr = new StreamReader("C:/Users/Public/Fractal.save");

                while ((line = sr.ReadLine()) != null)
               {
                   if (line.Contains("xzoom = ")) xzoom = Convert.ToDouble(line.Substring(8));
                   else if (line.Contains("yzoom = ")) yzoom = Convert.ToDouble(line.Substring(8));
                   else if (line.Contains("xstart = ")) xstart = Convert.ToDouble(line.Substring(9));
                   else if (line.Contains("ystart = ")) ystart = Convert.ToDouble(line.Substring(9));
               }
               sr.Close();
           }
           catch (Exception) {      // handle errors on load and warn user
               if (MessageBox.Show("Your file is either incorrectly formatted or you have not saved the fractal state yet. Click Ok to attempt to load the file anyway or Cancel and attempt to save the fractal again", "File failed to load properly",
               MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk) == DialogResult.Cancel) return;
           }
           mandelbrot();
           Refresh();
       }

       private void exportToolStripMenuItem_Click(object sender, EventArgs e)
       {
           SaveFileDialog saveFileDialog = new SaveFileDialog();                            // Create new save file dialog object
           saveFileDialog.Filter = "Jpeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";   // Specify file types
           saveFileDialog.Title = "Save Fractal Image File";                                // Give it a Title
           saveFileDialog.ShowDialog();                                                     // Display
           if (saveFileDialog.FileName != "")                                               // Check filename is not empty and ready to save
           {
               System.IO.FileStream fs = (System.IO.FileStream)saveFileDialog.OpenFile();   // Saves the Image through a FileStream
               switch (saveFileDialog.FilterIndex)                                          // Save image depending on file type selected by user
               {
                   case 1:
                       picture.Save(fs, System.Drawing.Imaging.ImageFormat.Jpeg);
                       break;

                   case 2:
                       picture.Save(fs, System.Drawing.Imaging.ImageFormat.Bmp);
                       break;

                   case 3:
                       picture.Save(fs, System.Drawing.Imaging.ImageFormat.Gif);
                       break;
               }
               fs.Close();                                                                  // Close the file stream
           }
       }

       private void startToolStripMenuItem_Click(object sender, EventArgs e)                // [color cycling - start] menu option starts timer1 tick method which runs the method at set intervals
       {
           startToolStripMenuItem.Checked = true;
           timer1.Start();
       }

       private void stopToolStripMenuItem_Click(object sender, EventArgs e)                 // [color cycling - stop] menu option stops the timer1 tick method running
       {
           startToolStripMenuItem.Checked = false;
           timer1.Stop();
       }

       private void timer1_Tick(object sender, EventArgs e)                                  // at set intervals create a new image and assign color-cycled fractal picture and update graphics to display
       {
           Image image = colorcycle(picture);
           g1 = Graphics.FromImage(picture);
           g1.DrawImage(image, 0, 0);
           Refresh();
       }

    }
}