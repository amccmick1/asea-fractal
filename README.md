# Fractal

Conversion of a program which produces a zoom-able Fractal bitmap from Java to C# with added functionality

## Extended C# Fractal Features and Functionality

All further updates to the Fractal program are extended features and functionality ...

#### Update v0.1.7 - 25/10/2014

Updated Save State to save variables to file
Updated Load State to read these variables from a file 

Load state is now able to load a saved state even if you exit the program after saving and open the program again. This loaded state is re-zoom-able

#### Update v0.1.6 - 22/10/2014

Implemented Color Cycling

Color cycling takes the pallette of the bitmap and shifts the entries by one each time it runs therefore cycling through the colors displayed by the Fractal image. In order to implement live color cycling I have openned a MemoryStream to write data to the computers internal memory (RAM) and used a Windows Form Timer to run the color cycling process at set intervals (tick). Four methods are used to run color cycling:

*colorcycle(Bitmap bitmap)
*timer1.Start()
*timer1.Stop()
*timer1_Tick()

The timer1.Start() and timer1.Stop() methods are called by the click event handler of the Color cycling -> Start and Color cycling -> Stop menu options respectively.

Once the timer starts it processes the timer1_Tick method at the set interval which creates a new image and assigns the fractal image returned by the colorcycle() method. It then draws the image to form and uses the Refresh() method to repaint the Fractal displayed on the form at every tick.

The colorcycle() method itself works by opening a MemoryStream to save the current Fractal bitmap as a gif  in the stream and then creates a new Bitmap using the data stored in the stream and moves the palette entries along one whilst assigning the first color to the next entry. The method then returns the new color cycled bitmap to the timer1_Tick() method which draws the new image onto the form.

#### Update v0.1.5 - 22/10/2014

Code clean up and re-factoring

Removed Edit -> Undo and Edit -> Redo menu options and replaced with Color Cycling -> Start and Color Cycling -> Stop
Added click event handlers for Color Cycling menu options
Renamed Form1 as Fractal

#### Update v0.1.4 - 22/10/2014

Added Export method. This method exports the bitmap Fractal image through a filestream as either a .jpg, .bmp or .gif depending on user selection in the Windows File Dialog prompt

#### Update v0.1.3 - 22/10/2014

Added Load State method. This method loads the four variables saved  by the Save State needed to compute Mandelbrot and allows re-zooming afterwards.

#### Update v0.1.2 - 22/10/2014

Added Save State method. This method saves the four variables needed to compute Mandelbrot and therefore call these values from Load State to draw the Fractal in a state which allows for re-zooming

Added variables:
*double xstartSaved, ystartSaved, xzoomSaved, yzoomSaved

#### Update v0.1.1 - 22/10/2014

Added Copy to Clipboard method. This method simply copies the bitmap to the Windows clipboard. This is a simple function which is pre-written in .net and is called by Clipboard.SetImage(picture)

#### Update v0.1.0 - 22/10/2014

Implemented new menu with blank click event handlers to C# Fractal program:
*File
	- Copy to Clipboard 	
	- Save State
	- Load State
	- Export
*Edit
	- Undo
	- Redo
	
## Java to C# Fractal Conversion

The original Java Fractal program converted to C#

#### Update v0.0.8 - v0.0.9 - 21/10/2014

Added cursor changes originally in Java to C# Mandelbrot() method
Added Form1 click events Form1_MouseDown, Form1_MouseUp and Form1_MouseMove to replace Java mousePressed() and mouseDragged() methods
Replaced Java repaint() method with in-built C# method Refresh()
Added mouseDragged flag in Form1_MouseDown, Form1_MouseUp Form1_MouseMove event handlers to replace Java mouseDragged() Java method

Added C# variables to replace Java functionality:
*bool mouseDragged

Full code compiles and runs program with all Java Fractal functionality implemented.

#### Update v0.0.7 - 21/10/2014

Replaced Java paint() method with C# Form1Paint() method and added PaintEventHandler to form through Form1 designer. Paint method is called through:
Form1 -> Events :: Appearance : Paint

#### Update v0.0.6 - 21/10/2014

Added paint() and update() methods from Java to C#
Uncommented Mandelbrot() method call from start() method as it now compiles
Replaced Java drawImage() method with C# DrawImage() method
Replaced Java g.setColor(Color.white) method with C# p = new Pen(Color.White)
Replaced Java g.drawRect() method with C# g.DrawRectangle() C# method

Added C# variables to match Java
*int xs, xe, ys, ye

#### Update v0.0.5 - 21/10/2014

Added provided HSBColor C# class and changed FromHSB() method from static in HSBColor class.
Changed constructor to times hsb values by 255 to bring output into rgb value range.

Added C# variables:
*HSBColor HSBcol = newHSBColor()
*Pen p

Replaced Java g1.setColor(col) method with C# p = new Pen(myColor)

Mandelbrot() now compiles

#### Update v0.0.4 - 21/10/2014

Added Mandelbrot() Java method and pointcolor() sub-method, commented out compile errors.

Added C# variables to match Java:
*int MAX

#### Update v0.0.3 - 21/10/2014

Added C# variables to draw to GDI which will be called later:

*Image picture
*Graphics g1

Replaced setSize() Java method with built in C# .net method calls:
- x1 = this.ClientRectangle.Width
- y1 = this.ClientRectangle.Height

Replaced createImage(x1, y1) Java method with picture = new Bitmap(x1, y1)

init() now compiles

#### Update v0.0.2 - 21/10/2014

Added C# variables to match Java:

*bool action, rectangle
*int x1, y1
*double xstart, ystart, xende, yende, xzoom, yzoom
*double SX = -2.0.25
*double SY = -1.125
*double EX = 0.6
*double EY = 1.125
*float xy

start() + initvalues() now compile with Mandelbrot() method call commented out.

#### Update v0.0.1 - 20/10/2014

Transferred Java Source Code to C# and commented out compile errors